define(function() {

    /*
    Usage:
    di.add(name, constructor) - Adds constructor to dependency list.
    di.literal(name, object) - Adds a literal object to dependency list.
    di.get(name, arguments, singleton) - Gets the dependency (name of, any args, is singleton class). If you dont have args,
    you can just leave it blank or have a boolean to set singleton still. When you get a literal, by default it's set to
    a singleton.
    di.injectList - Injects a list of dependencies in to supplied object / constructor, but will be default
    set each instanced class to be a singleton, it also doesnt accept arguments at the moment. It's a quick way to bind multiple
    classes though.
    */

    var nJect = function() {};

    nJect.prototype = {
        version: "0.2",
        deps: {},
        _singletons: [],
        _cache: [],
        _getSingleton: function(name) {
            for (var i = 0; i < this._singletons.length; i += 1) {
                if (this._singletons[i].name === name || this._singletons[i].dependencyName === name) return this._singletons[i];
            }
        },
        add: function(name, construct) {
            this.deps[name] = construct;
        },
        literal: function(name, obj) {
            obj.name = name;
            obj.isLiteral = true;
            this._singletons.push(obj);
        },
        get: function(name, args, requireSingleton) {

            var constructor, singleton, object;

            if (typeof args === "boolean") {
                requireSingleton = args;
                args = [];
            }

            if (!args) args = [];

            singleton = this._getSingleton(name);
            if (singleton && requireSingleton || singleton && singleton.isLiteral) return singleton;

            constructor = this.deps[name];
            if (!constructor) throw "Dependency rejected: Could not find '" + name + "' component.";

            object = Object.create(constructor.prototype);
            object.dependencyId = this.makeId();
            object.dependencyName = name;
            constructor.apply(object, args);

            if (requireSingleton) this._singletons.push(object);

            if (object) this._cache.push(object);

            return object;

        },
        injectList: function(dest, arr) {

            var i = 0,
                len = arr.length,
                currentItem;

            for (i; i < len; i += 1) {
                currentItem = arr[i];
                dest[currentItem] = this.get(currentItem, true);
            }

        },
        makeId: function() {
            return ("000000" + (Math.random() * Math.pow(36, 6)).toString(36)).slice(-6);
        },
        viewResources: function() {
            return {
                dependencies: this.deps,
                singletons: this._singletons,
                cache: this._cache
            };
        }
    };

    return new nJect();

});